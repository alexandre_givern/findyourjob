from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'index/', views.index, name='front_index'),
    url(r'map/', views.map, name='front_map'),
    url(r'login/', views.signin, name='front_signin'),
    url(r'signup/', views.signup, name='front_signup'),
    url(r'signout/', views.signout, name='front_signout'),
    url(r'forgetpassword/', views.forgetpassword, name='front_forgetpassword'),
]
