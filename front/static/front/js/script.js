/**
 * Google map API parameters and initialisation
 * The map appear on the element with the id 'map'
 **/
var map;

var mapOption = {
    center: {lat: -34.397, lng: 150.644},
    zoom: 15,
    mapTypeControl: false,
    styles : [
        {featureType: 'administrative',
        stylers: [
            {
                visibility: 'on'
            }
        ]},
        {featureType: 'landscape',
        stylers: [
            {
                visibility: 'on'
            }
        ]},
        {featureType: 'road',
        stylers: [
            {
                visibility: 'on'
            }
        ]},
        {featureType: 'transit',
        stylers: [
            {
                visibility: 'on'
            }
        ]},
        {featureType: 'water',
        stylers: [
            {
                visibility: 'on'
            }
        ]},
        {featureType: 'poi',
        stylers: [
            {
                visibility: 'off'
            }
        ]}
    ]
};


function initMap() {
    map = new google.maps.Map(document.getElementById('map'), mapOption);
}

jQuery(document).ready(function(){

    var isMobile = false;

    if (jQuery('.js-mobile-pixel').css('display') == 'none')
    {
        isMobile = true;
    }

    jQuery('.js-arrow').on('click', function(){
        jQuery('.js-arrow').show();
        jQuery(this).hide();


        if (jQuery(this).data('direction') == 'left')
        {
            jQuery('.js-navigation').width('40px');
            if (isMobile)
            {
                jQuery('.js-navigation').width('40px');
            }
        }
        else
        {
            jQuery('.js-navigation').width('25%');
            if (isMobile)
            {
                jQuery('.js-navigation').width('100%');
                jQuery('.js-navigation').height('100%');
            }
        }
    });

});
