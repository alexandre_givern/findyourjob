from django.shortcuts import render, HttpResponseRedirect, redirect
from django.urls import reverse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

import logging

@login_required
def index(request):
    """ Index page """
    return render(request, 'front/index.html', locals())

def map(request):
    """ Map page """
    return render(request, 'front/map.html', locals())

def signin(request):
    """ Signin page """
    iStep = 1

    if request.method == 'POST':
        sUsername = request.POST['username']
        sPassword = request.POST['password']
        if sUsername is None or sPassword is None:
            messages.error(request, 'All fields must be completed.')
            return render(request, 'front/signin.html', locals())

        else:
            oUser = authenticate(request, username=sUsername, password=sPassword)
            if oUser:
                login(request, oUser)
                return redirect(reverse('front_index'))
            else:
                messages.error(request, 'The username and password are not matching.')
                return render(request, 'front/signin.html', locals())

    else:
        return render(request, 'front/signin.html', locals())

def signup(request):
    """ Sign up management """
    iStep = 2

    if request.method == 'POST':
        sEmail = request.POST['email']
        sUsername = request.POST['username']
        sPassword = request.POST['password']
        sRpassword = request.POST['rpassword']
        if sEmail is None or sUsername is None or sPassword is None or sRpassword is None:
            messages.error(request, 'All fields must be completed.')
            return render(request, 'front/signin.html', locals())

        elif sPassword != sRpassword:
            messages.error(request, 'Incorrect password.')
            return render(request, 'front/signin.html', locals())

        else:
            try:
                validate_email(sEmail)
                oUser = User.objects.get(username=sUsername)
                messages.error(request, 'The username already exists.')
                return render(request, 'front/signin.html', locals())

            except ValidationError:
                messages.error(request, 'The email is incorrect.')
                return render(request, 'front/signin.html', locals())

            except User.DoesNotExist:
                user = User.objects.create_user(
                    sUsername,
                    sEmail,
                    sPassword
                )
                return redirect(reverse('front_index'))

    else:
        return render(request, 'front/signin.html', locals())

def signout(request):
    """ logout management """
    logout(request)
    return redirect(reverse('front_signin'))

def forgetpassword(request):
    """ Forget password management """
    return render(request, 'front/signin.html', locals())
